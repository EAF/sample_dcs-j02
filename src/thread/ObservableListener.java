package thread;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;

import observer.Observateur;
import socket.CupptSocket;

public class ObservableListener extends Observable implements Runnable {

	// for I/O
	private CupptSocket socket;
	
	private String message;

	// Observators collection
	private ArrayList<Observateur> listObservateur = new ArrayList<Observateur>();

	public ObservableListener(CupptSocket hostSocket) {
		
		this.socket = hostSocket;
		
	}

	public CupptSocket getSocket() {
		return this.socket;
	}

	public void run() {
		read();
	}

	public void read() {

		try {
			
			BufferedReader in = this.socket.getInput();

			String xml = "";
			int b = 0;

			do {

				b = in.read();
				xml += (char) b;

			} while (this.socket.getInput().ready());


			this.message = xml;
			
			this.updateObservateur();

			if (b!=-1) {
				read();
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());

		} catch (Exception e) {
			System.out.println(e.getMessage());

		}

	}

	// Ajoute un observateur � la liste
	public void addObservateur(Observateur obs) {
		this.listObservateur.add(obs);
	}

	// Retire tous les observateurs de la liste
	public void delObservateur() {
		this.listObservateur = new ArrayList<Observateur>();
	}

	public void updateObservateur() {
		for (Observateur obs : this.listObservateur)
			obs.update(this.message);
	}
}
