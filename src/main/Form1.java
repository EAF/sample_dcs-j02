package main;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import observer.Observateur;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.wb.swt.SWTResourceManager;

import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import socket.CupptSocket;
import thread.ObservableListener;

public class Form1 {

	protected Shell shlCupptSdk;
	protected Display display;

	private Text textCUPPSPLT, textCUPPSPN, textCUPPSOPR, textCUPPSCN, textCUPPSPP, textXSDU,
			textCUPPSUN, textCUPPSACN;

	private StyledText styledTextGoals, styledTextXML, styledTextEvent;

	private Button btnInterfacelevelsavailablerequest, btnConnection;

	TabFolder tabFolder;
	TabItem tabPageLesson1, tabPageLesson2;

	private Table tableEvents;

	private CupptSocket platformSocket = new CupptSocket();

	// XSD informations
	String XSDLevel;
	String XSDLocalPath;
	String XSDVersion;

	private Label label_1;
	private Button btnUnsupportedInterfacelevelrequest;
	private Button btnObsoleteInterfacelevelrequest;
	private Label label_2;
	private Table tableInterfaceLevels;
	private TableColumn tableColumn;
	private TableColumn tableColumn_1;
	private TableColumn tableColumn_2;
	private Button btnInterfacelevelrequest;
	private StyledText styledTextXMLValidate;
	private Label label_3;
	private Button btnDisconnect;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Form1 window = new Form1();
			window.open();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shlCupptSdk.open();
		shlCupptSdk.layout();
		while (!shlCupptSdk.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();

			}
		}

	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlCupptSdk = new Shell();
		shlCupptSdk.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {

				System.out.println("closing...");
				disconnect();

			}
		});

		shlCupptSdk.setImage(SWTResourceManager.getImage(Form1.class, "/main/icon53.png"));
		shlCupptSdk.setMinimumSize(new Point(971, 649));
		shlCupptSdk.setSize(760, 432);
		shlCupptSdk.setText("CUPPT SDK - lesson 02 Java");

		Group grpPlatformConnection = new Group(shlCupptSdk, SWT.NONE);
		grpPlatformConnection.setText("Platform Connection");
		grpPlatformConnection.setBounds(10, 10, 935, 105);

		Label lblSitecuppsplt = new Label(grpPlatformConnection, SWT.NONE);
		lblSitecuppsplt.setBounds(10, 22, 101, 15);
		lblSitecuppsplt.setText("Site (CUPPSPLT)");

		textCUPPSPLT = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPLT.setBounds(117, 19, 89, 21);

		Label lblNodecuppspn = new Label(grpPlatformConnection, SWT.NONE);
		lblNodecuppspn.setText("Node (CUPPSPN)");
		lblNodecuppspn.setBounds(212, 22, 101, 15);

		textCUPPSPN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPN.setBounds(319, 19, 89, 21);

		Label lblOpratorcuppsopr = new Label(grpPlatformConnection, SWT.NONE);
		lblOpratorcuppsopr.setText("Operator (CUPPSOPR)");
		lblOpratorcuppsopr.setBounds(414, 22, 123, 15);

		textCUPPSOPR = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSOPR.setBounds(543, 19, 89, 21);

		Label lblComputerNamecuppscn = new Label(grpPlatformConnection, SWT.NONE);
		lblComputerNamecuppscn.setText("Computer Name (CUPPSCN)");
		lblComputerNamecuppscn.setBounds(651, 22, 160, 15);

		textCUPPSCN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSCN.setBounds(817, 19, 108, 21);

		Label lblPortcuppspp = new Label(grpPlatformConnection, SWT.NONE);
		lblPortcuppspp.setText("Port (CUPPSPP)");
		lblPortcuppspp.setBounds(212, 49, 101, 15);

		textCUPPSPP = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPP.setBounds(319, 46, 89, 21);

		Label lblXsdSchema = new Label(grpPlatformConnection, SWT.NONE);
		lblXsdSchema.setText("XSD");
		lblXsdSchema.setBounds(212, 77, 101, 15);

		textXSDU = new Text(grpPlatformConnection, SWT.BORDER);
		textXSDU.setBounds(319, 74, 311, 21);

		Label lblUsernamecuppsun = new Label(grpPlatformConnection, SWT.NONE);
		lblUsernamecuppsun.setText("UserName (CUPPSUN)");
		lblUsernamecuppsun.setBounds(414, 49, 123, 15);

		textCUPPSUN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSUN.setBounds(543, 46, 89, 21);

		Label lblAlternateNamecuppsacn = new Label(grpPlatformConnection, SWT.NONE);
		lblAlternateNamecuppsacn.setText("Alternate Name (CUPPSACN)");
		lblAlternateNamecuppsacn.setBounds(651, 49, 160, 15);

		textCUPPSACN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSACN.setBounds(817, 46, 108, 21);

		tabFolder = new TabFolder(shlCupptSdk, SWT.NONE);
		tabFolder.setBounds(10, 121, 658, 336);

		tabPageLesson1 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson1.setText("Lesson 1");

		Composite compositeLesson1 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson1.setControl(compositeLesson1);

		btnConnection = new Button(compositeLesson1, SWT.NONE);
		btnConnection.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonConnect_Click(e);
			}
		});
		btnConnection.setAlignment(SWT.LEFT);

		btnConnection.setBounds(443, 36, 197, 25);
		btnConnection.setText("1 - Network Socket Connection");

		btnInterfacelevelsavailablerequest = new Button(compositeLesson1, SWT.NONE);
		btnInterfacelevelsavailablerequest.setEnabled(false);
		btnInterfacelevelsavailablerequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonInterfacesLevelAvailableRequest_Click(e);
			}
		});
		btnInterfacelevelsavailablerequest.setAlignment(SWT.LEFT);
		btnInterfacelevelsavailablerequest.setText("2 - InterfaceLevelsAvailableRequest");
		btnInterfacelevelsavailablerequest.setBounds(443, 67, 197, 25);

		styledTextXML = new StyledText(compositeLesson1, SWT.BORDER);
		styledTextXML.setBounds(10, 36, 427, 100);
		styledTextXML.setWordWrap(true);

		styledTextEvent = new StyledText(compositeLesson1, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		styledTextEvent.setBounds(10, 142, 630, 156);
		styledTextEvent.setWordWrap(true);
		styledTextEvent.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				styledTextEvent.setTopIndex(styledTextEvent.getLineCount() - 1);
			}
		});

		Label label = new Label(compositeLesson1, SWT.NONE);
		label.setText("XML Message");
		label.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		label.setAlignment(SWT.CENTER);
		label.setBounds(10, 15, 427, 15);

		btnDisconnect = new Button(compositeLesson1, SWT.NONE);
		btnDisconnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnDisconnect_Click(e);
			}
		});
		btnDisconnect.setText("Disconnect");
		btnDisconnect.setBounds(443, 111, 197, 25);

		tabPageLesson2 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson2.setText("Lesson 2");

		Composite compositeLesson2 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson2.setControl(compositeLesson2);

		label_1 = new Label(compositeLesson2, SWT.NONE);
		label_1.setText("InterfaceLevelErrorEvent");
		label_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		label_1.setBounds(443, 70, 197, 15);

		btnUnsupportedInterfacelevelrequest = new Button(compositeLesson2, SWT.NONE);
		btnUnsupportedInterfacelevelrequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnUnsuportedInterfacelevelrequest_Click(e);
			}
		});
		btnUnsupportedInterfacelevelrequest.setText("Unsupported InterfaceLevelRequest");
		btnUnsupportedInterfacelevelrequest.setForeground(SWTResourceManager.getColor(255, 0, 0));
		btnUnsupportedInterfacelevelrequest.setEnabled(false);
		btnUnsupportedInterfacelevelrequest.setBackground(SWTResourceManager.getColor(255, 0, 0));
		btnUnsupportedInterfacelevelrequest.setAlignment(SWT.LEFT);
		btnUnsupportedInterfacelevelrequest.setBounds(443, 91, 197, 25);

		btnObsoleteInterfacelevelrequest = new Button(compositeLesson2, SWT.NONE);
		btnObsoleteInterfacelevelrequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonObsoleteInterfacesLevelRequest_Click(e);
			}
		});
		btnObsoleteInterfacelevelrequest.setText("Obsolete InterfaceLevelRequest");
		btnObsoleteInterfacelevelrequest.setForeground(SWTResourceManager.getColor(255, 0, 0));
		btnObsoleteInterfacelevelrequest.setEnabled(false);
		btnObsoleteInterfacelevelrequest.setBackground(SWTResourceManager.getColor(255, 0, 0));
		btnObsoleteInterfacelevelrequest.setAlignment(SWT.LEFT);
		btnObsoleteInterfacelevelrequest.setBounds(443, 122, 197, 25);

		label_2 = new Label(compositeLesson2, SWT.NONE);
		label_2.setText("Interface Levels Available List");
		label_2.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		label_2.setBounds(10, 10, 187, 15);

		tableInterfaceLevels = new Table(compositeLesson2, SWT.BORDER | SWT.FULL_SELECTION);
		tableInterfaceLevels.setLinesVisible(true);
		tableInterfaceLevels.setHeaderVisible(true);
		tableInterfaceLevels.setBounds(10, 27, 427, 120);

		tableColumn = new TableColumn(tableInterfaceLevels, SWT.NONE);
		tableColumn.setWidth(55);
		tableColumn.setText("Level");

		tableColumn_1 = new TableColumn(tableInterfaceLevels, SWT.NONE);
		tableColumn_1.setWidth(264);
		tableColumn_1.setText("Local Path");

		tableColumn_2 = new TableColumn(tableInterfaceLevels, SWT.NONE);
		tableColumn_2.setWidth(100);
		tableColumn_2.setText("XSD Version");

		btnInterfacelevelrequest = new Button(compositeLesson2, SWT.NONE);
		btnInterfacelevelrequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonInterfacesLevelRequest_Click(e);
			}
		});
		btnInterfacelevelrequest.setText("3 - InterfaceLevelRequest");
		btnInterfacelevelrequest.setEnabled(false);
		btnInterfacelevelrequest.setAlignment(SWT.LEFT);
		btnInterfacelevelrequest.setBounds(443, 27, 197, 25);

		styledTextXMLValidate = new StyledText(compositeLesson2, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL);
		styledTextXMLValidate.setWordWrap(true);
		styledTextXMLValidate.setBounds(10, 170, 630, 130);

		label_3 = new Label(compositeLesson2, SWT.NONE);
		label_3.setText("XML Message Response Validation");
		label_3.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		label_3.setBounds(10, 153, 187, 15);

		tableEvents = new Table(shlCupptSdk, SWT.BORDER | SWT.FULL_SELECTION);
		tableEvents.setBounds(10, 463, 658, 138);
		tableEvents.setHeaderVisible(true);
		tableEvents.setLinesVisible(true);

		TableColumn tblclmnTime = new TableColumn(tableEvents, SWT.NONE);
		tblclmnTime.setWidth(186);
		tblclmnTime.setText("Time");

		TableColumn tblclmnEvent = new TableColumn(tableEvents, SWT.NONE);
		tblclmnEvent.setWidth(463);
		tblclmnEvent.setText("Event");

		Group grpLessonGoals = new Group(shlCupptSdk, SWT.NONE);
		grpLessonGoals.setText("Lesson Goals");
		grpLessonGoals.setBounds(674, 137, 271, 464);

		styledTextGoals = new StyledText(grpLessonGoals, SWT.BORDER);
		styledTextGoals.setBackground(SWTResourceManager.getColor(255, 255, 204));
		styledTextGoals.setBounds(10, 23, 251, 400);
		styledTextGoals.setWordWrap(true);

		loadEnvironmentVariables();
		loadGoals();

	}

	// load Environment Variables
	private void loadEnvironmentVariables() {
		try {
			textCUPPSPN.setText(System.getenv("CUPPSPN"));
			textCUPPSPP.setText(System.getenv("CUPPSPP"));

			textCUPPSPLT.setText(System.getenv("CUPPSPLT"));
			textCUPPSCN.setText(System.getenv("CUPPSCN"));
			textCUPPSACN.setText(System.getenv("CUPPSACN"));

			textCUPPSOPR.setText(System.getenv("CUPPSOPR"));
			textCUPPSUN.setText(System.getenv("CUPPSUN"));

			textXSDU.setText(System.getenv("CUPPSXSDU"));
		} catch (Exception ec) {
			tableEventAddLine("Error getting Environment variables:" + ec);
		}

	}

	// load text goals
	private void loadGoals() {
		styledTextGoals.append("1. Send XML Message \n");
		styledTextGoals.append("\n");
		styledTextGoals.append("2. Wait for response and manage errors \n");
		styledTextGoals.append("   invalid XML message\n");
		styledTextGoals.append("   invalid CUPPT message\n");
		styledTextGoals.append("\n");
	}

	private void buttonConnect_Click(SelectionEvent e) {

		try {
			if (!platformSocket.getSocket().isConnected() || platformSocket.getSocket().isClosed()) {

				String IP = textCUPPSPN.getText();

				int port = Integer.parseInt(textCUPPSPP.getText());

				platformSocket = new CupptSocket(IP, port, "platform");

				tableEventAddLine("Connected to " + IP + ":" + port);

				// Init ObservableListener
				final ObservableListener observablePlatformListener = new ObservableListener(
						platformSocket);

				platformSocket.setObservableListener(observablePlatformListener);

				// Add observator

				platformSocket.getObservableListener().addObservateur(new Observateur() {
					public void update(String message) {

						final String XML = message;

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {

								CupptSocket socket = observablePlatformListener.getSocket();

								styledTextEventAppend('R', XML);

								tableEventAddLine("Message " + socket.getIP() + ":"
										+ socket.getPort() + " <-- " + XML);

								XMLReader(XML, platformSocket);
							}
						});

					}
				});

				btnInterfacelevelsavailablerequest.setEnabled(true);

				generateXML();

			} else {
				dialog("Already connected", SWT.ICON_WARNING);
			}
		} catch (Exception ec) {
			tableEventAddLine("Error connectiong to server:" + ec);
			disconnect();
		}

	}

	private void buttonInterfacesLevelAvailableRequest_Click(SelectionEvent e) {
		try {

			tableInterfaceLevels.removeAll();

			sendXML("interfaceLevelsAvailableRequest");

		} catch (Exception E) {
			System.out.println(E.toString());
		}
	}

	private void buttonInterfacesLevelRequest_Click(SelectionEvent e) {

		if (tableInterfaceLevels.getSelectionCount() > 0) {

			//
			for (TableItem selectedItem : tableInterfaceLevels.getSelection()) {

				XSDLevel = selectedItem.getText(0);
				XSDLocalPath = selectedItem.getText(1);
				XSDVersion = selectedItem.getText(2);

			}

			generateXML("interfaceLevelRequest");

			// send message
			SendMsg(styledTextXML.getText());

		} else {

			// ICON_ERROR, ICON_INFORMATION, ICON_QUESTION, ICON_WARNING,
			// ICON_WORKING

			dialog("Please select Interface Level", SWT.ICON_INFORMATION | SWT.OK);
			/*
			 * MessageBox dialog = new MessageBox(shlCupptSdk,
			 * SWT.ICON_INFORMATION | SWT.OK);
			 * 
			 * dialog.setText("Airline Application");
			 * dialog.setMessage("Please select Interface Level");
			 * dialog.open();
			 */

		}
	}

	private void buttonObsoleteInterfacesLevelRequest_Click(SelectionEvent e) {

		XSDLevel = "01.00";
		XSDLocalPath = "01.00";
		XSDVersion = "01.00";

		generateXML("interfaceLevelRequest");

		// send message
		SendMsg(styledTextXML.getText());

	}

	private void btnUnsuportedInterfacelevelrequest_Click(SelectionEvent e) {
		XSDLevel = "01.02";
		XSDLocalPath = "01.02";
		XSDVersion = "01.02";

		generateXML("interfaceLevelRequest");

		// send message
		SendMsg(styledTextXML.getText());
	}

	private void SendMsg(String XML) {
		SendMsg(XML, platformSocket);
	}

	private void SendMsg(String XML, CupptSocket socket) {
		try {

			if (XML.contains("{MESSAGEID}"))
				socket.incrementMessageID();

			// replace XML vars
			XML = replace(XML, "{MESSAGEID}", String.valueOf(socket.getMessageID()));
			XML = replace(XML, "{XSDVERSION}", XSDVersion);
			XML = replace(XML, "{INTERFACELEVEL}", XSDLevel);

			// send header msg
			XML = generateMsgHeader(XML) + XML;

			styledTextEventAppend('S', XML);

			tableEventAddLine("Message " + socket.getIP() + ":" + socket.getPort() + " --> " + XML);

			socket.send(XML);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String generateMsgHeader(String XML) throws UnsupportedEncodingException {
		// send header msg
		String version = "01";
		String validCode = "00";
		String size = "" + Integer.toHexString(XML.getBytes("utf-8").length);

		System.out.println("XML Size : " + XML.length() + ", Hex : " + size);

		while (size.length() < 6)
			size = "0" + size;

		return version + validCode + size.toUpperCase();
	}

	private void btnDisconnect_Click(SelectionEvent e) {
		disconnect();

		tableEventAddLine("Disconnected to " + platformSocket.getIP() + ":"
				+ platformSocket.getPort());
	}

	private void clear() {

		styledTextXML.setText("");
		styledTextEvent.setText("");

	}

	/*
	 * Close the Input/Output streams and disconnect not much to do in the catch
	 * clause
	 */
	private void disconnect() {
		try {

			platformSocket.disconnect();

			clear();
		} catch (Exception e) {
			System.out.println("exception");

		} // not much else I can do

	}

	private int dialog(String Message, int OPTIONS) {

		// ICON_ERROR, ICON_INFORMATION, ICON_QUESTION, ICON_WARNING,
		// ICON_WORKING
		MessageBox dialog = new MessageBox(shlCupptSdk, OPTIONS);

		dialog.setText("Airline Application");
		dialog.setMessage(Message);

		return dialog.open();

	}

	private void tableEventAddLine(String text) {

		TableItem item = new TableItem(tableEvents, SWT.NONE);
		item.setText(0, getDate());
		item.setText(1, text);

		tableEvents.select(tableEvents.getItemCount() - 1);
		tableEvents.showSelection();

		System.out.println(text);
	}

	private void styledTextEventAppend(char T, String str) {

		String symbol = (T == 'S') ? "-->" : "<--";

		styledTextEvent.append(T + " " + symbol + " " + getDate() + " :\n");

		styledTextEvent.append(getDate() + " :\n");
		styledTextEvent.append(str);
		styledTextEvent.append("\n\r");

		System.out.println(str);
	}

	// donne la date avec un format par défaut
	private String getDate() {
		return getDate("dd/MM/yyyy HH:mm:ss.SSS");
	}

	// surcharge de la fonction précédente avec le choix du format
	private String getDate(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(new Date());
	}

	private void sendXML(String XML) {
		sendXML(XML, platformSocket);
	}

	// override function
	private void sendXML(String XML, CupptSocket socket) {

		generateXML(XML);
		// send message
		SendMsg(styledTextXML.getText(), socket);
	}

	private void generateXML() {

		String type = "interfaceLevelsAvailableRequest";
		generateXML(type);
	}

	private void generateXML(String type) {
		// header
		String XML = "<cupps messageID = \"{MESSAGEID}\" xmlns:xsi = \"http://www.w3.org/2001/XMLSchema-instance\" messageName = \""
				+ type + "\">";

		if (type.equals("interfaceLevelsAvailableRequest")) {
			XML += "<interfaceLevelsAvailableRequest hsXsdVersion = \"{XSDVERSION}\"/>";
		} else if (type.equals("interfaceLevelRequest")) {
			XML += "<interfaceLevelRequest level=\"{INTERFACELEVEL}\"/>";
		}

		// end
		XML += "</cupps>";

		styledTextXML.setText(XML);

	}

	private String replace(String originalText, String subStringToFind,
			String subStringToReplaceWith) {
		int s = 0;
		int e = 0;

		StringBuffer newText = new StringBuffer();

		while ((e = originalText.indexOf(subStringToFind, s)) >= 0) {

			newText.append(originalText.substring(s, e));
			newText.append(subStringToReplaceWith);
			s = e + subStringToFind.length();

		}

		newText.append(originalText.substring(s));
		return newText.toString();

	}

	private void XMLReader(String rmessage, CupptSocket socket) {

		int startIndex = rmessage.indexOf('<');

		if (startIndex > -1) {
			try {

				// String prefix = rmessage.substring(0, startIndex);
				// System.out.println("prefix : '" + prefix + "'");

				String XML = rmessage.substring(rmessage.indexOf('<'));

				styledTextXMLValidate.setText("");
				// parse an XML document into a DOM tree
				DocumentBuilder parser;
				parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();

				// Document document = parser.parse(new File("instance.xml"));
				Document document = parser.parse(new InputSource(new StringReader(XML)));

				// show parsed message
				for (int i = 0; i < document.getElementsByTagName("*").getLength(); i++) {

					Node node = document.getElementsByTagName("*").item(i);

					if (node.getNodeType() == Node.ELEMENT_NODE) {

						styledTextXMLValidate.append(node.getNodeName() + "\n");

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {
								Node attr = attrs.item(j);
								styledTextXMLValidate.append("\t" + attr.getNodeName() + ": "
										+ attr.getNodeValue() + "\n");
							}
						}
					}
				}

				Node node = document.getElementsByTagName("cupps").item(0);
				String message = node.getAttributes().getNamedItem("messageName").getNodeValue();

				if (message.equals("interfaceLevelsAvailableResponse")) {

					NodeList nodeList = document.getElementsByTagName("interfaceLevel");

					for (int i = 0; i < nodeList.getLength(); i++) {

						node = nodeList.item(i);

						btnInterfacelevelrequest.setEnabled(true);
						btnObsoleteInterfacelevelrequest.setEnabled(true);
						btnUnsupportedInterfacelevelrequest.setEnabled(true);

						tabFolder.setSelection(1);

						ArrayList<String> attr = new ArrayList<String>();

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {
								attr.add(attrs.item(j).getNodeValue());
							}
						}

						TableItem item = new TableItem(tableInterfaceLevels, SWT.NONE);
						item.setText(0, attr.get(0));
						item.setText(1, attr.get(1));
						item.setText(2, attr.get(2));
					}

				} else if (message.equals("notify")) {

					String response = "notify \n";
					String textContent = "";

					NodeList nodeList = document.getElementsByTagName("notify").item(0)
							.getChildNodes();
					for (int i = 0; i < nodeList.getLength(); i++) {
						node = nodeList.item(i);

						response += "NodeName : " + node.getNodeName() + "\n";

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {

								Node attr = attrs.item(j);
								response += "    " + attr.getNodeName();

								if (attr.getNodeName().length() <= 11) {
									response += "\t";
								}
								if (attr.getNodeName().length() <= 6) {
									response += "\t";
								}
								response += ": " + attr.getNodeValue() + "\n";
							}
						}

						if (!node.getTextContent().isEmpty()
								& !textContent.equals(node.getTextContent())) {
							response += "textContent : " + node.getTextContent() + "\n";
							textContent = node.getTextContent();

						}
					}

					dialog(response, SWT.ICON_WORKING);
				}

				// styledTextXMLValidate.setText(text);

			} catch (ParserConfigurationException e1) {

				styledTextXMLValidate.setText("ParserConfigurationException : " + e1.getMessage());

			} catch (SAXParseException spe) {

				StringBuffer sb = new StringBuffer(spe.getMessage());
				sb.append("\n\tLine number: " + spe.getLineNumber());
				sb.append("\n\tColumn number: " + spe.getColumnNumber());

				styledTextXMLValidate.setText("SAXParseException : " + sb.toString());

			} catch (SAXException e1) {
				styledTextXMLValidate.setText("SAXException : " + e1.getMessage());
			} catch (IOException e1) {
				styledTextXMLValidate.setText("IOException : " + e1.getMessage());
			} catch (Exception e) {
				dialog(e.toString(), SWT.ICON_ERROR);
			}
		}
	}

	/*  */

}
